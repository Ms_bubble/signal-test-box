/*
@file: libManager.cpp
@author: ZZH
@time: 2022-09-29 17:26:00
@info: 外部库管理器
*/
#include "libManager.h"
#include "symTable.h"

QMap<QString, LibManager_t::LibOps> LibManager_t::libMap;

bool LibManager_t::LoadLib(const QString& libName)
{
    COMP_INFO("load lib: %s", libName.toStdString().c_str());
    QLibrary lib(QString("lib/%1").arg(libName.split('.')[0]));
    if (!lib.load())
    {
        COMP_INFO("lib %s load error: %s", libName.toStdString().c_str(), lib.errorString().toStdString().c_str());
        return false;
    }

    COMP_INFO("load lib succ");
    auto pfInit = reinterpret_cast<LibManager_t::LinInitFun_t>(lib.resolve("lib_init"));//解析lib_init函数, 作为库入口点
    if (nullptr != pfInit)
    {
        auto libFuncs = pfInit();
        while (nullptr != libFuncs->sym)//不为空则导入下一个
        {
            const char* finalName = nullptr == libFuncs->name ? libFuncs->sym : libFuncs->name;
            auto pf = lib.resolve(libFuncs->sym);
            if (nullptr != pf)
            {
                COMP_INFO("load fun: %s", finalName);
                FunSymTable.insert(finalName, {
                    reinterpret_cast<ASTFunctionCall_t::pf>(pf), libFuncs->argNum
                });
            }
            else
            {
                COMP_INFO("load %s function error", finalName);
            }
            libFuncs++;
        }

        auto pfExit = lib.resolve("lib_exit");
        libMap.insert(libName, { pfInit, pfExit });
    }

    return true;
}


