%locations

%{
#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include "ast.h"
#include <QtCore>
#include <QMap>
#include <QMessageBox>
#include "symTable.h"
#include "compiler.h"
int yyerrorCount = 0;
extern yyrestart(FILE* f);
%}

%code requires
{
#include "ast.h"
#define inFileLoc(arg) arg.first_line, arg.first_column, arg.last_line, arg.last_column
extern int yylex (void);
extern int yyerror(const QString& s);
inline int yyerror(const char* s) { return yyerror(QString::fromUtf8(s)); }
extern void resetParser();
extern size_t yycolumn;
extern int yylineno;
extern int yyerrorCount;
}

%union{
    ASTExpress_t *node;//语法树节点
    QList<ASTExpress_t*> *list;
    float value;//浮点数值
    char* id;//符号的名称
}

%destructor {
    if(nullptr != $$)
    {
        // COMP_INFO("Parser delete a node");
        delete $$;
    }
} <node>

%destructor {
    if(nullptr != $$)
    {
        for(auto p: *$$)
        {
            // COMP_INFO("Parser delete a node in list");
            delete p;
        }
        // COMP_INFO("Parser delete a list");
        delete $$;
    }
} <list>

%destructor {
    if(nullptr != $$)
    {
        // COMP_INFO("Parser delete a string");
        free($$);
    }
} <id>

%token <value> tk_Number "number"
%token <id> tk_Symbol "symbol"
%token <id> tk_String "string"

%token tk_kwIndex "index"
%token tk_Time "time"

%token tk_kwIf "if"
%token tk_kwEles "else"

%token tk_opGequ ">="
%token tk_opLequ "<="
%token tk_opEqu "=="
%token tk_opNequ "!="

//优先级低
%right "else"
%right "if"
%left '^'
%left '<' '>' "==" "!=" ">=" "<="
%left '+' '-'
%left '*' '/' '%'
/* %right '!' '~' */
//优先级高

%type <value> constant_exp
%type <node> string else_exp
%type <node> signal signal_exp signal_call func_call condition_exp variable_exp compare_exp
%type <list> arg_list


%%

signal: signal_exp {$$ = $1; Compiler_t::getInst().setASTRoot($1); return 0;}

/* 信号表达式 */
signal_exp: constant_exp {$$ = new ASTNumber_t($1);}
    |variable_exp
    |condition_exp
    |compare_exp

/* 可变表达式, 不是变量表达式, 意思是其值需要在运行期才能求出的表达式 */
variable_exp: "time" {$$ = new ASTTime_t();}
    |'(' variable_exp ')' {$$ = $2;}
    |"index" {$$ = new ASTIndex_t();}
    |func_call
    |signal_call
/* 可变表达式和常量表达式之间的运算 */
    |variable_exp '+' constant_exp {$$ = new ASTOperator_t('+', $1, new ASTNumber_t($3));}
    |variable_exp '-' constant_exp {$$ = new ASTOperator_t('-', $1, new ASTNumber_t($3));}
    |variable_exp '*' constant_exp {$$ = new ASTOperator_t('*', $1, new ASTNumber_t($3));}
    |variable_exp '/' constant_exp {$$ = new ASTOperator_t('/', $1, new ASTNumber_t($3));}
    |variable_exp '%' constant_exp {$$ = new ASTOperator_t('%', $1, new ASTNumber_t($3));}
    |variable_exp '^' constant_exp {$$ = new ASTOperator_t('^', $1, new ASTNumber_t($3));}
/* 反方向 */
    |constant_exp '+' variable_exp {$$ = new ASTOperator_t('+', new ASTNumber_t($1), $3);}
    |constant_exp '-' variable_exp {$$ = new ASTOperator_t('-', new ASTNumber_t($1), $3);}
    |constant_exp '*' variable_exp {$$ = new ASTOperator_t('*', new ASTNumber_t($1), $3);}
    |constant_exp '/' variable_exp {$$ = new ASTOperator_t('/', new ASTNumber_t($1), $3);}
    |constant_exp '%' variable_exp {$$ = new ASTOperator_t('%', new ASTNumber_t($1), $3);}
    |constant_exp '^' variable_exp {$$ = new ASTOperator_t('^', new ASTNumber_t($1), $3);}
/* 可变表达式和可变表达式之间的运算 */
    |variable_exp '+' variable_exp {$$ = new ASTOperator_t('+', $1, $3);}
    |variable_exp '-' variable_exp {$$ = new ASTOperator_t('-', $1, $3);}
    |variable_exp '*' variable_exp {$$ = new ASTOperator_t('*', $1, $3);}
    |variable_exp '/' variable_exp {$$ = new ASTOperator_t('/', $1, $3);}
    |variable_exp '%' variable_exp {$$ = new ASTOperator_t('%', $1, $3);}
    |variable_exp '^' variable_exp {$$ = new ASTOperator_t('^', $1, $3);}

/* 常量表达式, 不是不可变表达式, 意思是其值在编译期就可求出的表达式 */
constant_exp: "number"
    |'(' constant_exp ')' {$$ = $2;}
    |constant_exp '+' constant_exp {$$ = $1 + $3;}
    |constant_exp '-' constant_exp {$$ = $1 - $3;}
    |constant_exp '*' constant_exp {$$ = $1 * $3;}
    |constant_exp '/' constant_exp {$$ = $1 / $3;}
    |constant_exp '%' constant_exp {$$ = std::fmod($1, $3);}
    |constant_exp '^' constant_exp {$$ = powf($1, $3);}
    |constant_exp '>' constant_exp {$$ = $1 > $3;}
    |constant_exp '<' constant_exp {$$ = $1 < $3;}
    |constant_exp "==" constant_exp {$$ = $1 == $3;}
    |constant_exp "!=" constant_exp {$$ = $1 != $3;}
    |constant_exp ">=" constant_exp {$$ = $1 >= $3;}
    |constant_exp "<=" constant_exp {$$ = $1 <= $3;}

signal_call: "symbol" {
    $$ = nullptr;
    SignalItem* tmp;
    bool res = SigSymTable.search($1, tmp);

    if(true == res)
    {
        $$ = new ASTAdaptor_t(tmp);
        Compiler_t::getInst().queuePush(tmp);
    }
    else
    {
        $$ = nullptr;
        yyerror(QString::fromUtf8("使用了未定义的信号: %1").arg($1));
    }

    free($1);
}

func_call: "symbol" '(' arg_list ')' {
    ASTFunctionCall_t::calFunc_t find_func;
    bool res = FunSymTable.search($1, find_func);
    
    if(true == res)
    {
        unsigned int len = 0;
        
        if(nullptr != $3)
            len = $3->length();

        if(len == find_func.numOfArg)//传入的实际参数和要求的参数一致
        {
            $$ = new ASTFunctionCall_t(find_func.pFunc, $3);
        }
        else
        {
            yyerror(QString::fromUtf8("传入的参数数量与函数 %1 要求的不一致").arg($1));
        }
    }
    else
    {
        $$ = nullptr;
        if(nullptr != $3)
        {
            for(auto it: *$3)
                delete it;

            delete $3;
        }
        yyerror(QString::fromUtf8("调用了未定义的函数: %1").arg($1));
    }

    free($1);
}

/* 比较表达式 */
compare_exp: variable_exp '>' variable_exp {$$ = new ASTOperator_t('>', $1, $3);}
    |variable_exp '<' variable_exp {$$ = new ASTOperator_t('<', $1, $3);}
    |variable_exp "==" variable_exp {$$ = new ASTCompare_t(ASTCompare_t::EQU, $1, $3);}
    |variable_exp "!=" variable_exp {$$ = new ASTCompare_t(ASTCompare_t::NEQU, $1, $3);}
    |variable_exp ">=" variable_exp {$$ = new ASTCompare_t(ASTCompare_t::GEQU, $1, $3);}
    |variable_exp "<=" variable_exp {$$ = new ASTCompare_t(ASTCompare_t::LEQU, $1, $3);}

    |constant_exp '>' variable_exp {$$ = new ASTOperator_t('>', new ASTNumber_t($1), $3);}
    |constant_exp '<' variable_exp {$$ = new ASTOperator_t('<', new ASTNumber_t($1), $3);}
    |constant_exp "==" variable_exp {$$ = new ASTCompare_t(ASTCompare_t::EQU, new ASTNumber_t($1), $3);}
    |constant_exp "!=" variable_exp {$$ = new ASTCompare_t(ASTCompare_t::NEQU, new ASTNumber_t($1), $3);}
    |constant_exp ">=" variable_exp {$$ = new ASTCompare_t(ASTCompare_t::GEQU, new ASTNumber_t($1), $3);}
    |constant_exp "<=" variable_exp {$$ = new ASTCompare_t(ASTCompare_t::LEQU, new ASTNumber_t($1), $3);}
    
    |variable_exp '>' constant_exp {$$ = new ASTOperator_t('<', $1, new ASTNumber_t($3));}
    |variable_exp '<' constant_exp {$$ = new ASTOperator_t('>', $1, new ASTNumber_t($3));}
    |variable_exp "==" constant_exp {$$ = new ASTCompare_t(ASTCompare_t::EQU, $1, new ASTNumber_t($3));}
    |variable_exp "!=" constant_exp {$$ = new ASTCompare_t(ASTCompare_t::NEQU, $1, new ASTNumber_t($3));}
    |variable_exp ">=" constant_exp {$$ = new ASTCompare_t(ASTCompare_t::GEQU, $1, new ASTNumber_t($3));}
    |variable_exp "<=" constant_exp {$$ = new ASTCompare_t(ASTCompare_t::LEQU, $1, new ASTNumber_t($3));}

    |'(' compare_exp ')' {$$ = $2;}

condition_exp: signal_exp "if" compare_exp else_exp {$$ = new ASTCondition_t($3, $1, $4);}
    | '(' condition_exp ')' {$$ = $2;}

else_exp: {$$ = new ASTNumber_t(0);}
    |"else" signal_exp {$$ = $2;}

arg_list: {$$ = nullptr;}
    |signal_exp {$$ = new QList<ASTExpress_t*>(); $$->append($1);}
    |arg_list ',' signal_exp {$$ = $1; $$->append($3);}
    |string {$$ = new QList<ASTExpress_t*>(); $$->append($1);}
    |arg_list ',' string {$$ = $1; $$->append($3);}

string: "string" {$$ = new ASTString_t($1); }//free($2);} 不free掉是因为后面是传指针

%%

int yyerror(const QString& s)
{
    /* printf("error occor:%s : %d.%d-%d.%d\r\n", s, inFileLoc(yylloc)); */
    QMessageBox::critical(nullptr, QString::fromUtf8("编译错误"), 
    QString::fromUtf8("%1\n位于 %2.%3-%4.%5").arg(s).arg(yylloc.first_line).arg(yylloc.first_column).arg(yylloc.last_line).arg(yylloc.last_column)
    );
    yyerrorCount++;
    return 0;
}

void resetParser()
{
    yylineno = 1;
    yycolumn = 1;
    yyerrorCount = 0;
    yyrestart(nullptr);
}
