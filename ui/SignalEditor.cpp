/*
@file: SignalEditor.cpp
@author: ZZH
@time: 2022-10-31 13:15:59
@info: 信号编辑器
*/
#include "SignalEditor.h"

void SignalEditor::setUp()
{
    UI_INFO("signal editor init");
    this->setAcceptDrops(true);
}

// void SignalEditor::dragEnterEvent(QDragEnterEvent* e)
// {
//     if (e->mimeData()->hasText())
//         e->acceptProposedAction();
//     else
//         QTextEdit::dragEnterEvent(e);
// }

// void SignalEditor::dropEvent(QDropEvent* e)
// {
//     this->append(e->mimeData()->data("text/plain"));

//     QTextEdit::dropEvent(e);
// }
