<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="zh_CN">
<context>
    <name>MainWindow</name>
    <message>
        <source>MainWindow</source>
        <translation type="vanished">主窗口</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="14"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="277"/>
        <source>SignalBox</source>
        <translation>信号实验箱</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="29"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="288"/>
        <source>Sample Freq:</source>
        <translation>采样率:</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="64"/>
        <location filename="mainWindow.ui" line="73"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="291"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="294"/>
        <source>KHz</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="68"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="290"/>
        <source>Hz</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="78"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="292"/>
        <source>MHz</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="99"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="295"/>
        <source>Sample Points:</source>
        <translation>采样点数:</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="124"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="297"/>
        <source>For some reason, sample points maximum value is limited to 1024, greater value will be support later</source>
        <translation>出于某些原因, 采样点数目前最大限制为1024, 后续版本将会支持更大的值</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="161"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="299"/>
        <source>Signal List:</source>
        <translation>信号列表:</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="246"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="302"/>
        <source>Signal Expression:</source>
        <translation>信号表达式:</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="292"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="303"/>
        <source>Calculate</source>
        <translation>计算当前信号</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="305"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="304"/>
        <source>Spectrum Mode</source>
        <translation>频谱模式</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="318"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="278"/>
        <source>Add Signal</source>
        <translation>新建信号</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="323"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="279"/>
        <source>Delete Signal</source>
        <translation>删除信号</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="328"/>
        <location filename="mainWindow.ui" line="331"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="280"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="282"/>
        <source>Export Workspace</source>
        <translation>导出工作区</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="336"/>
        <location filename="mainWindow.ui" line="339"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="284"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="286"/>
        <source>Import Workspace</source>
        <translation>导入工作区</translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="195"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="300"/>
        <source>+</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainWindow.ui" line="214"/>
        <location filename="../build/signalBox_autogen/include/ui_mainwindow.h" line="301"/>
        <source>-</source>
        <translation></translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="68"/>
        <source>select file to load</source>
        <translation>选择要导入的文件</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="72"/>
        <location filename="mainWindow.cpp" line="79"/>
        <location filename="mainWindow.cpp" line="87"/>
        <location filename="mainWindow.cpp" line="104"/>
        <location filename="mainWindow.cpp" line="126"/>
        <source>file error</source>
        <translation>文件错误</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="72"/>
        <location filename="mainWindow.cpp" line="126"/>
        <source>not select a file.</source>
        <translation>没有选择文件</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="79"/>
        <location filename="mainWindow.cpp" line="133"/>
        <source>can`t open: %1</source>
        <translation>无法打开: %1</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="87"/>
        <location filename="mainWindow.cpp" line="104"/>
        <source>file format error</source>
        <translation>文件格式错误</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="122"/>
        <source>select file to save</source>
        <translation>选择要保存到的文件</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="133"/>
        <source>file open fail</source>
        <translation>文件打开失败</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="172"/>
        <location filename="mainWindow.cpp" line="249"/>
        <source>Name duplicate</source>
        <translation>命名重复</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="172"/>
        <source>Auto name could only try 32 times, if your workspace have a lot of auto-named signal, this problem will happen, but you can try add signal again to fix it.</source>
        <translation>自动命名只会尝试32次, 如果你的工作区内已经存在大量自动命名的信号, 就有可能会出现此问题, 但是你可以再次尝试添加信号来修复它</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="249"/>
        <source>This name is same as another signal name which already exists.</source>
        <translation>此名称与现有信号名称重复</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="254"/>
        <source>Name illegal</source>
        <translation>名称非法</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="254"/>
        <source>This name not match naming rules.</source>
        <translation>此名称不符合信号命名规则</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="273"/>
        <source>Calculate point error</source>
        <translation>计算点数错误</translation>
    </message>
    <message>
        <location filename="mainWindow.cpp" line="274"/>
        <source>%1 is a illegal value (valid range[%2 - %3])</source>
        <oldsource>%1 is illegal value (range[%2 - %3])</oldsource>
        <translation>%1 不在有效范围内 (有效范围 [%2 - %3])</translation>
    </message>
</context>
</TS>
