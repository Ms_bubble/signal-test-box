/*
@file: SignalEditor.h
@author: ZZH
@time: 2022-10-31 13:16:14
@info: 信号编辑器
*/
#pragma once
#include <QTextEdit>
#include <QDragEnterEvent>
#include <QDropEvent>
#include <QMimeData>
#include "log.h"

class SignalEditor:public QTextEdit
{
private:

protected:

public:
    explicit SignalEditor(QWidget* parent = nullptr): QTextEdit(parent) { this->setUp(); }
    explicit SignalEditor(const QString &text, QWidget *parent = nullptr): QTextEdit(text, parent) { this->setUp(); }
    ~SignalEditor() {}

    void setUp(void);

    // void dragEnterEvent(QDragEnterEvent *e) override;
    // void dropEvent(QDropEvent* e) override;
};

